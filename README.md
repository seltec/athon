# AthON
![alt text](https://www.seltec-sports.net/TAF/Athon.png "Seltec AthON Logo")

The "Athletics Objection Notation" (AthON) format is an OpenAPI/Swagger based JSON specification to exchange athletics data between providers.

## What  is the current status

AthON is currently in DRAFT state and open for contribution.

## What can be transmitted with AthON?

* A single competition or multiple competitions
    * Only basedata (like name, town...), possible with venue informations
    * Competition-structure 
    * With entries
    * With entries and complete results
* A single athlete or multiple athletes
    * Their basedata for ID resolving
    * Their best performances: Seasonbest and Personalbest 

## How to test AthON?

For the moment SELTEC provides a small test instance to GET some testdata from our results server.
Head over to https://test.laportal.net/swagger to test some features of AthON.

Please keep in mind, this is a testserver and will be sometimes out of sync with the specification version published here.

For example you can try to query the legacy competition with ID 17:
`curl -X GET "https://test.laportal.net/api/Results/v1/legacy?id=17" -H "accept: application/json"`

## How to use AthON?

As a provider wishing to receive AthON formatted data, you do NOT need to implement the full feature set of AthON. A subset implementation of action paths (and according to the action path only the respective models) is part of the reason why AthON exists.
Athletics data is very complex and most providers will treat them a bit differently. AthONs target is to find the most minimal data compatibility level, but have a nearly full feature set for every optionality. Because of this, most of the fields are NOT marked as required.

## Local/Provider Extensions

AthON will allow local/provider extensions to exchange special data inside the format. Local/provider extension fields should use the underscore_IOC_ or underscore_provider_ notation. 

E.g: If the italian federation wants to include the shoesize for athletes, add `"_ita_shoesize"="42"`

E.g: For the world athletics athlete identification we recommend  `"_WA_id"="foo123bar456"`
Which is already included in the current draft

Please share local extensions also with us! Every provider should just skip local extension fields if the semantic is unknown.

## Identification of entities

If an entity (Can be everything: Athlete, Club or even a single Performance) should be traced across systems, a unique ID should be provided.
For IDs we recommend 2 semantics (represented in the fieldname):

* ID (or as an extension _xxx_ID) should be treated as a string, containing all UTF-8 allowed characters
* GUID (or as an extension _xxx_GUID) should be treated as a GUID represented in the RFC4122 recommended 8-4-4-4-12 format

If one or both id systems are in use, depends heavily on the provider system and the local available data.

## Contribution

We invite everyone to contribute to the AthON format. Please open an issue here in Bitbucket or send an email to athon@seltec-sports.com.

## License

The AthON specification and all documentation and auxiliary files inside this repository are licensed under the EUPL-1.2-or-later.
For the text of the license in English, see [LICENSE.md](LICENSE.md).
For other languages, see https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12

## Copyright

The AthON specification and all documentation and auxiliary files are copyright (c) by SELTEC GmbH